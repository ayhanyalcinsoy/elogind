#!/usr/bin/python
# -*- coding: utf-8 -*-

# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import mesontools

def setup():
    mesontools.configure("--prefix=/usr \
                          --libexecdir=/usr/lib \
                          -Dadmin_group=wheel \
                          -Delogind=true \
                          -Dsystemdsystemunitdir=no \
                          -Dgtk_doc=true \
                          -Drootlibexecdir=/usr/lib/accountsservice")

def build():
    mesontools.build()

def install():
    mesontools.install()