#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 2.
# See the file http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt

from pisi.actionsapi import get
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools


def setup():
    # /var/run => /run
    #for f in ["configure.ac", "src/Makefile.am", "src/Makefile.in"]:
        #pisitools.dosed(f, "\$\(?localstatedir\)?(\/run\/(\$PACKAGE|NetworkManager))", "\\1")
    #pisitools.dosed("configure.ac", "\/var(\/run\/ConsoleKit)", "\\1")
    #pisitools.dosed("configure.ac", "^initscript", deleteLine=True)
    autotools.autoreconf("-fiv")
    shelltools.system("intltoolize --force --copy --automake")

    autotools.configure("--with-nmtui \
                         --disable-lto \
                         --disable-ifnet \
                         --disable-static \
                         --enable-ppp=yes \
                         --enable-concheck \
                         --sysconfdir=/etc \
                         --with-crypto=nss \
                         --enable-bluez5-dun \
                         --without-netconfig \
                         --localstatedir=/var \
                         --disable-silent-rules \
                         --enable-modify-system \
                         --with-modem-manager-1 \
                         --disable-more-warnings \
                         --with-udev-dir=/lib/udev \
                         --with-pppd=/usr/sbin/pppd \
                         --disable-config-plugin-ibft \
                         --with-suspend-resume=elogind \
                         --with-dhcpcd=/usr/bin/dhcpcd \
                         --with-iptables=/sbin/iptables \
                         --with-systemdsystemunitdir=no \
                         --with-session-tracking=elogind \
                         --with-dnsmasq=/usr/sbin/dnsmasq \
                         --with-dhclient=/usr/sbin/dhclient \
                         --libexecdir=/usr/lib/NetworkManager \
                         --with-system-ca-path=/etc/ssl/certs \
                         --disable-session-tracking-consolekit \
                         --with-dbus-sys-dir=/etc/dbus-1/system.d \
                         --with-kernel-firmware-dir=/lib/firmware \
                         --with-pppd-plugin-dir=/usr/lib/pppd/2.4.7 \
                         --with-resolvconf=/etc/resolv.default.conf \
                        ")

    # fix unused direct dependency analysis
    pisitools.dosed("libtool", " -shared ", " -Wl,-O1,--as-needed -shared ")

def build():
    autotools.make()

#def check():
    #autotools.make("-k check")

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())

    pisitools.dodir("/etc/NetworkManager/VPN")

    pisitools.dodoc("COPYING*", "README")