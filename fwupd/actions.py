#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import mesontools

def setup():
    mesontools.configure("--prefix=/usr \
                          --sysconfdir=/etc \
                          --localstatedir=/var \
                          --libexecdir=/usr/lib/fwupd \
                          -Dman=false \
                          -Dtests=true \
                          -Dgtkdoc=false \
                          -Delogind=true \
                          -Dsystemd=false \
                          -Dconsolekit=false \
                          -Dplugin_tpm=false \
                          -Dplugin_dell=false \
                          -Dplugin_uefi=false \
                          -Dplugin_nvme=false \
                          -Dplugin_flashrom=false \
                          -Dplugin_synaptics=false \
                          -Dplugin_modem_manager=true \
                          -Defi-includedir=/usr/include/efi")
    
def build():
    mesontools.build()

def install():
    mesontools.install()