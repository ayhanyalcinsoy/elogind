#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 2.
# See the file http://www.gnu.org/copyleft/gpl.txt.

from pisi.actionsapi import get
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools


def setup():
    # fix error "on't know how to bootstrap submodule 'lib/ivykis'"
    #shelltools.unlinkDir("lib/ivykis")
    #pisitools.dosed("autogen.sh", "SUBMODULES=\"lib/ivykis lib/jsonc\"", "SUBMODULES=\"\"")
    #shelltools.system("./autogen.sh")
    autotools.autoreconf("-vif")
    autotools.configure("--enable-sql \
                         --prefix=/usr \
                         --enable-ipv6 \
                         --disable-java \
                         --disable-amqp \
                         --disable-smtp \
                         --disable-kafka \
                         --with-python=3 \
                         --disable-redis \
                         --disable-geoip2 \
                         --enable-manpages \
                         --disable-riemann \
                         --disable-mongodb \
                         --sbindir=/usr/bin \
                         --with-jsonc=system \
                         --with-ivykis=system \
                         --datadir=/usr/share \
                         --enable-all-modules \
                         --enable-spoof-source \
                         --libexecdir=/usr/lib \
                         --disable-java-modules  \
                         --sysconfdir=/etc/syslog-ng \
                         --with-pidfile-dir=/var/run \
                         --with-systemdsystemunitdir=no \
                         --localstatedir=/var/lib/syslog-ng")

    # fix unused direct dependency analysis
    pisitools.dosed("libtool", " -shared ", " -Wl,-O1,--as-needed -shared ")

def build():
    autotools.make()

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())

    pisitools.dodoc("COPYING", "README*")