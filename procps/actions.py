#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools

def setup():
    shelltools.system("./autogen.sh")
    autotools.configure("--prefix=/usr \
                         --disable-kill \
                         --with-elogind \
                         --exec-prefix=/ \
                         --disable-static \
                         --sysconfdir=/etc \
                         --libdir=/usr/lib \
                         --bindir=/usr/bin \
                         --without-systemd \
                         --sbindir=/usr/bin \
                         --enable-watch8bit \
                         --disable-modern-top")

    # fix unused direct dependency analysis
    pisitools.dosed("libtool", " -shared ", " -Wl,-O1,--as-needed -shared ")

def build():
    autotools.make()

def install():
    autotools.rawInstall('ln_f="ln -sf" ldconfig="true" lib64=lib DESTDIR=%s' % get.installDIR())
    
    #remove conflicts
    pisitools.remove("/usr/bin/pidof")
    pisitools.remove("/usr/share/man/man1/pidof.1")
    
    # for mudur and comar
    pisitools.dosym("/usr/bin/sysctl", "/sbin/sysctl")
    pisitools.dosym("/usr/bin/ps", "/bin/ps")

    #pisitools.dosym("libproc-%s.so" % get.srcVERSION(), "/lib/libproc.so")

    pisitools.dodoc("COPYING", "ps/HACKING")