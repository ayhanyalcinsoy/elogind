#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import libtools
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools

# FIXME: libpulsedsp.la should be added, but it doesn't build on our system
emul32_libs = "libpulse.la \
               libpulse-simple.la \
               libpulsecommon-%s.la \
               libpulse-mainloop-glib.la \
              " % get.srcVERSION()

shelltools.export('SYSTEMDLOGIN_LIBS', 'pkg-config --libs "libelogind" 2>/dev/null')
shelltools.export('SYSTEMDLOGIN_CFLAGS', 'pkg-config --cflags "libelogind" 2>/dev/null')

def setup():
    options = "--with-speex \
               --prefix=/usr \
               --enable-gconf \
               --disable-rpath \
               --disable-bluez4 \
               --disable-static \
               --sysconfdir=/etc \
               --enable-largefile \
               --with-database=tdb \
               --disable-oss-output \
               --localstatedir=/var \
               --enable-systemd-login \
               --with-system-user=pulse \
               --with-system-group=pulse \
               --with-systemduserunitdir=no \
               --disable-dependency-tracking \
               --libexecdir=/usr/lib/pulseaudio \
               --with-access-group=pulse-access \
               --with-udev-rules-dir=/lib/udev/rules.d \
               --with-module-dir=/usr/lib/pulse/modules"

    if get.buildTYPE() == "emul32":
        options += " --disable-x11 \
                     --disable-gtk2 \
                     --disable-lirc \
                     --disable-gconf \
                     --disable-bluez4 \
                     --disable-asyncns \
                     --disable-solaris \
                     --disable-manpages \
                     --libdir=/usr/lib32 \
                     --disable-samplerate \
                     --disable-oss-output \
                     --disable-oss-wrapper \
                     --libexecdir=/usr/lib32 \
                     --disable-default-build-tests"

    shelltools.echo(".tarball-version", get.srcVERSION())
    #shelltools.system("NOCONFIGURE=1 ./bootstrap.sh")
    autotools.configure(options)

    pisitools.dosed("libtool", "CC(\s-shared\s)", r"CC -Wl,-O1,--as-needed\1")

def build():
    if get.buildTYPE() == "emul32":
        autotools.make("-C src %s" % emul32_libs)
        return

    autotools.make()

    #generate html docs
    autotools.make("doxygen")

def install():
    if get.buildTYPE() == "emul32":
        autotools.rawInstall("-C src \
                              lib_LTLIBRARIES=\"%s\" \
                              DESTDIR=%s" % (emul32_libs, get.installDIR()),
                             "install-libLTLIBRARIES")
        autotools.rawInstall("DESTDIR=%s" % get.installDIR(), "install-pkgconfigDATA")
        return

    autotools.rawInstall("DESTDIR=%s" % get.installDIR())

    # Disable autospawn by default
    shelltools.system("sed -e '/autospawn/iautospawn=yes' -i '%s/etc/pulse/client.conf'" % get.installDIR())
    # Speed up pulseaudio shutdown
    # Lower resample quality, saves CPU
    shelltools.system("sed -e '/exit-idle-time/iexit-idle-time=0' \
                       -e '/resample-method/iresample-method=speex-float-0' \
                       -i '%s/etc/pulse/daemon.conf'" % get.installDIR())

    # Needed for service.py
    pisitools.dodir("/run/pulse")
    pisitools.dodir("/var/lib/pulse")

    # HAL is no longer supported by default
    pisitools.removeDir("/etc/dbus-1")

    pisitools.dodoc("README", "LICENSE", "GPL", "LGPL")
    pisitools.dohtml("doxygen/html/*")
