#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools

def setup():
    pisitools.dosed("configure.ac", '(AS_AC_EXPAND\(EXPANDED_LOCALSTATEDIR, )"\$localstatedir"\)', r'\1 "")')
    for f in ["bus/Makefile.am", "bus/Makefile.in"]:
        pisitools.dosed(f, "\$\(localstatedir\)(\/run\/dbus)", "\\1")
    options = "--disable-tests \
               --disable-static \
               --disable-checks \
               --enable-inotify \
               --sysconfdir=/etc \
               --disable-selinux \
               --disable-asserts \
               --disable-systemd \
               --disable-xml-docs \
               --disable-libaudit \
               --localstatedir=/var \
               --with-dbus-user=dbus \
               --disable-doxygen-docs \
               --disable-silent-rules \
               --enable-user-session \
               --disable-modular-tests \
               --enable-x11-autolaunch \
               --disable-embedded-tests \
               --without-systemdsystemunitdir \
               --with-session-socket-dir=/tmp \
               --libexecdir=/usr/lib/dbus-1.0 \
               --with-system-pid-file=/run/dbus/pid \
               --with-console-auth-dir=/run/console/ \
               --with-system-socket=/run/dbus/system_bus_socket "

    if get.buildTYPE() != "emul32":
        options += "--enable-elogind"

    if get.buildTYPE() == "emul32":
        options += "--disable-xml-docs \
                    --disable-elogind  \
                    --disable-doxygen-docs"
        # Build only libdbus
        pisitools.dosed("Makefile.am", "(.*SUBDIRS=dbus) .*", "\\1")

    shelltools.system("NOCONFIGURE=1 ./autogen.sh")
    #autotools.autoreconf("-vif")
    autotools.configure(options)

def build():
    autotools.make()

def check():
    autotools.make("check")

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())
    if get.buildTYPE() == "emul32": return

    # needs to exist for the system socket
    pisitools.dodir("/run/dbus")
    pisitools.dodir("/var/lib/dbus")
    pisitools.dodir("/usr/share/dbus-1/services")

    pisitools.dodoc("AUTHORS", "ChangeLog", "NEWS", "README", "doc/TODO", "doc/*.txt")
    pisitools.dohtml("doc/")
